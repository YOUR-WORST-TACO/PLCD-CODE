--------------------
--LIFE BASE CLASS--
--------------------

LIFE = {}
LIFE.__index = LIFE

setmetatable( LIFE, {
	__index = OBJ,
	__call = function ( cls, ... )
		local self = setmetatable( {}, cls )
		self:_init( ... )
		return self
	end
} )

function LIFE:_init( limit, pos )
	self.help = "limit, pos"
	if pos == nil then
		print( self.help )
		return
	end
	OBJ._init( self, pos )
	self.mAge = limit
	self.state = true
end

function LIFE:Kill()
	self.state = false
end

function LIFE:Alive()
	return self.state
end

--------------------
--PLANT--
--------------------

PLANT = {}
PLANT.__index = PLANT

setmetatable( PLANT, {
	__index = LIFE,
	__call = function ( cls, ... )
		local self = setmetatable( {}, cls )
		self:_init( ... )
		return self
	end
} )

function PLANT:_init( size, anual, pos )
	self.help = "size, anual, pos"
	if size == nil then
		print( self.help )
		return
	end
	LIFE._init( self, 0, pos )
	self.size = size
	self.anual = anual
	self.curFood = size
	self._type = "PLANT"
	self.symb = "F"
end

--------------------
--ANIMAL--
--------------------

ANIMAL = {}
ANIMAL.__index = ANIMAL

setmetatable( ANIMAL, {
	__index = LIFE,
	__call = function ( cls, ... )
		local self = setmetatable( {}, cls )
		self:_init( ... )
		return self
	end
} )

function ANIMAL:_init( limit, pos )
	self.help = "limit, pos"
	if limit == nil then
		print( self.help )
		return
	end
	LIFE._init( self, limit, pos )
	self._type = "ANIMAL"
end

--------------------
--PREY--
--------------------

PREY = {}
PREY.__index = PREY

setmetatable( PREY, {
	__index = ANIMAL,
	__call = function ( cls, ... )
		local self = setmetatable( {}, cls )
		self:_init( ... )
		return self
	end
} )

function PREY:_init( limit, pos )
	self.help = "limit, pos"
	if limit == nil then
		print( self.help )
		return
	end
	ANIMAL._init( self, limit, pos )
	self._type = "PREY"
	self.symb = "v"
end

--------------------
--PRED--
--------------------

PRED = {}
PRED.__index = PRED

setmetatable( PRED, {
	__index = ANIMAL,
	__call = function ( cls, ... )
		local self = setmetatable( {}, cls )
		self:_init( ... )
		return self
	end
} )

function PRED:_init( limit, pos )
	self.help = "limit, pos"
	if limit == nil then
		print( self.help )
		return
	end
	ANIMAL._init( self, limit, pos )
	self._type = "PRED"
	self.symb = "O"
end