ECO = {}
ECO.__index = ECO

setmetatable( ECO, {
	__call = function ( cls, ... )
		local self = setmetatable( {}, cls )
		self:_init( ... )
		return self
	end
} )

function CurTime()
	return os.clock()
end

function ECO:_init( speed )
	if speed == nil then speed = 0.1 end
	self.uid = get_unique()
	self.grid = 1000000
	self.objects = {}
	self.NextThink = 0
	self.speed = speed
end

function ECO:GenerateGrid( size )
	local grid = {}
	for i = 1, size, 1 do
		table.insert( grid, i, " " )
		for k = 1, size, 1 do
			table.insert( grid[i], k, " " )
		end
	end
	return grid
end

function ECO:DisGrid()

end

function ECO:Start()
	while true do
		self:Think()
	end
end

function ECO:CanThink()
	if self.NextThink < CurTime() then
		self.NextThink = CurTime() + self.speed
		return true
	else
		return false
	end
end

function ECO:Think()
	if not self:CanThink() then return end

	--CALL ALL KNOWN THINK FUNCTIONS--
	for _, obj in pairs( self.objects ) do
		obj:Think()
	end
end

function ECO:Add( obj )
	table.insert( self.objects, obj )
end