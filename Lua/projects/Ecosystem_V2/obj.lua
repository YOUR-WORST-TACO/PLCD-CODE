OBJ = {}
OBJ.__index = OBJ

setmetatable( OBJ, {
	__call = function ( cls, ... )
		local self = setmetatable( {}, cls )
		self:_init( ... )
		return self
	end
} )

function OBJ:_init( pos )
	self.uid = get_unique()
	self.owner = getOwner()
	if type( pos ) == "number" then
		self.pos = { x = pos, y = pos }
	elseif type( pos ) == "table" and pos[1] ~= nil and pos[2] ~= nil then
		self.pos = { x = pos[1], y = pos[2] }
	elseif type( pos ) == "table" and pos["x"] ~= nil and pos["y"] ~= nil then
		self.pos = { x = pos["x"], y = pos["y"] }
	else
		self.pos = { x = 0, y = 0 }
	end
	self._type = "NONE"
	self.symb = " "
end

--BASIC THINK FUNCTION--
function OBJ:Think()
	print( "think", self.uid )
end

function OBJ:Sym( sym )
	if sym == nil then
		return self.symb
	else
		self.symb = sym
	end
end

function OBJ:GetPos()
	return self.pos
end

function OBJ:SetPos( x, y )
	if ( y == nil ) and type( x ) == "table" then
		self.pos = x
	else
		self.pos = { x = x, y = y }
	end
end

function OBJ:Owner()
	return self.owner
end

function OBJ:GetType()
	return self._type
end

function OBJ:ID()
	return self.uid
end