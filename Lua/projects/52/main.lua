function getFact( num )
	if num > 0 then
		return num * getFact( num - 1 )
	else
		return 1
	end
end

print( tostring( getFact( 52 ) ) )
