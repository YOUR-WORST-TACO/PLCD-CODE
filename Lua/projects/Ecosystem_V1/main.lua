--[[
MAIN FILE
]]--
require( 'eco' )
require( 'taco' )
	
u_id = 0

function curTime()
	return os.clock()
end

function uid()
	u_id = u_id + 1
	return u_id
end

function math.dist( x1, y1, x2, y2 )
	return ((x2-x1)^2+(y2-y1)^2)^0.5
end

function start()
	math.randomseed(os.time())

	ecosystem = ECO( 0.5, 0.1 )
	ecosystem:addPred( 40 ) 
	ecosystem:addPrey( 40 )
	ecosystem:addPrey( 40 )
	ecosystem:dropFood()
end
start()
while true do
	ecosystem:think()
end
