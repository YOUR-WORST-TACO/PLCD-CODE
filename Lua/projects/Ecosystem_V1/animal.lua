--[[
ANIMAL FILE
]]--

ANIMAL = {}
ANIMAL.__index = ANIMAL

setmetatable( ANIMAL, {
	__call = function ( cls, ... )
		local self = setmetatable( {}, cls )
		self:_init( ... )
		return self
	end
} )

function ANIMAL:_init( age )
	self.id = uid()
	self.age = function( age ) return age end
	self.agro = math.random( 1, 5 )
	self.speed = math.random( 300, 700 )
	if ecosystem ~= nil then
		self.dAge = ecosystem:getTime() + age
	else
		self.dAge = age
	end
	self.pos = {x=0,y=0}
	self.state = true
end

function ANIMAL:think()
end

function ANIMAL:findObj( _type, dis )
	local dist = { obj = nil, dist = 10000000000 }
	for k, v in pairs( ecosystem:Objects() ) do
		if v:Type() == _type and v ~= self then
			local pX, pY = v:getPos()
			local X, Y = self:getPos()
			local distance = math.dist( X,Y,pX,pY )
			if ( distance < dist.dist ) then
				dist = {obj = v, dist = distance}
			end
		end
	end
	if dist.dist <= dis then
		if ( dist.obj:Type() == "predator" or dist.obj:Type() == "prey" ) then
			if dist.obj:alive() then
				return dist.obj
			end
			return false
		else
			return dist.obj 
		end
	else
		return false
	end
end

function ANIMAL:wander()
	local x, y = self:getPos()
	local _x = math.random( -self.speed, self.speed )
	local _y = math.random( -self.speed, self.speed )
	local max = ecosystem:getSize()
	--COLLISION HANDLES--
	--X--
	if ( x <= self.speed ) then
		_x = math.abs( _x )
	elseif ( x >= max - self.speed ) then
		_x = -math.abs( _x )
	end
	--Y--
	if ( y <= self.speed ) then
		_y = math.abs( _y )
	elseif ( y >= max -self.speed ) then
		_y = -math.abs( _y )
	end
	x = x + _x
	y = y + _y
	--X HANDLES--
	if x > max then
		x = max 
	end
	if x <= 0 then
		x = 1
	end
	--Y HANDLES--
	if y > max then
		y = max
	end
	if y <= 0 then
		y = 1
	end
	--[[
	self.pos.x = x 
	self.pos.y = y
	]]--
	self:setPos( x, y )
end

function ANIMAL:alive()
	return self.state
end
--[[
function ANIMAL:checkPos()
	if self.pos.x < 1 then self.pos.x = 1 end
	if self.pos.y < 1 then self.pos.y = 1 end
	if self.pos.x > ecosystem:getSize() then self.pos.x = ecosystem:getSize() end
	if self.pos.y > ecosystem:getSize() then self.pos.y = ecosystem:getSize() end
end
]]--
function ANIMAL:kill()
	self.state = false
	self._type = "foodP"
end

function ANIMAL:ID()
	return self.id
end

function ANIMAL:getPos()
	return self.pos.x, self.pos.y
end

function ANIMAL:setPos( x, y )
	self.pos.x = math.floor( x )
	self.pos.y = math.floor( y )
end

function ANIMAL:getDage()
	return self.dAge
end

function ANIMAL:getAgro()
	return self.agro
end

function ANIMAL:getSpeed()
	return self.speed
end

function ANIMAL:Type()
	return self._type
end

function ANIMAL:setAge( val )
	self.age = val
end

function ANIMAL:setAgro( val )
	self.agro = val
end

function ANIMAL:setSpeed( val )
	self.speed = val
end
