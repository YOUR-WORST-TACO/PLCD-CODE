--[[
PREDATOR FILE
]]--

require( 'animal' )

PREDATOR = {}
PREDATOR.__index = PREDATOR

setmetatable( PREDATOR, {
	__index = ANIMAL,
	__call = function ( cls, ... )
		local self = setmetatable( {}, cls )
		self:_init( ... )
		return self
	end
} )

function PREDATOR:_init( age, agro, speed )
	ANIMAL._init( self, age, agro, speed )
	self._type = "predator"
	self.huntD = 10000
	self.speed = self.speed + 50
end

function PREDATOR:think()
	local prey = self:findObj( "prey", self.huntD )
	local decay = self:findObj( "foodP", self.huntD * 1.5 )
	local pred = self:findObj( "predator", self.huntD * 0.8 )
	if ( prey ) then
		self:goTo( self, prey )
	elseif ( decay ) then
		self:goTo( self, decay )
	elseif ( pred ) then
		if ( pred:getAgro() <= self.agro and self.agro > 4 ) then
			self:goTo( self, pred )
		else
			self:wander()
		end
	else
		self:wander()
	end
end

function PREDATOR:goTo( obj, targ )
	local x, y = obj:getPos()
	local _x, _y = targ:getPos()
	local Cx = ( _x - x )
	local Cy = ( _y - y )
	local h = math.sqrt( Cx^2 + Cy^2 )
	local speed = self.speed
	local r = speed / h
	if ( r < 1 ) then
		Cx = Cx * r
		Cy = Cy * r
		nX = self.pos.x + Cx
		nY = self.pos.y + Cy
		if nX < 1 then nX = 1 end
		if nY < 1 then nY = 1 end
		if nX > ecosystem:getSize() then nX = ecosystem:getSize() end
		if nY > ecosystem:getSize() then nY = ecosystem:getSize() end
		self:setPos( nX, nY )
	else
		self:setPos( _x, _y )
		self:attack( targ )
	end
end

function PREDATOR:attack( targ )
	local rand = math.random( 1, 1000 )
	if rand > 500 and targ:Type() =="prey" and ecosystem:ratio() <= 0.5 then
		ecosystem:addPred( 40, self:getPos() )
	end
	ecosystem:removeObj( targ )
	self.dAge = self.dAge + 1
end