--[[
ECOSYSTEM FILE
]]--

require( 'predator' )
require( 'prey' )
require( 'taco' )
require( 'object' )

ECO = {}
ECO.__index = ECO

setmetatable( ECO, {
	__call = function ( cls, ... )
		return cls.new(...)
	end
} )

function ECO.new( freq, speed )
	local self = setmetatable( {}, ECO )
	self.freq = freq
	self.id = uid()
	self.speed = speed
	self.time = 0
	self.size = 50000
	self.think_time = 0
	self.objects = {}
	self.food_time = 0
	return self
end

--THINK FUNCTION-- 
function ECO:think()
	math.randomseed(os.time())
	if self.think_time > curTime() then return end
	
	self.think_time = curTime() + self.speed
	if self:findPrey() == 0 and self:findPred() == 0 then
		self.objects = {}
		u_id = 0
		self:addPred( 40 ) 
		self:addPrey( 40 )
		self:addPrey( 40 )
	end
 	self:proTime()
 	--self:dropFood()
 	self.food_time = self.food_time + 1
	if self.food_time > 10 and self:findPrey() > 0 and self:findPred() > 0 then
		self.food_time = 0
		self:dropFood()
	end
	for k, obj in pairs( self.objects ) do
		if ( obj:Type() ~= "foodO" ) then
			if ( obj:getDage() < self.time ) then
				obj:kill()
			end
			if ( obj:alive() ) then
				obj:think()
			end
		end
	end

	self:disLand()
	
	
	--printTable( self.objects )
end

function ECO:dropFood()
	local object = FOOD()
	if ( object ~= nil ) then
		x = math.random( self.size )
		y = math.random( self.size )
		object:setX( x )
		object:setY( y )
		self.objects[ tostring(object:ID() ) ] = object
	end
end

--FUNCTIONS FOR ECOSYSTEMS--
function ECO:makeLand( size )
	local land = {}
	for i = 1, size, 1 do
		table.insert( land, {} )
		for k = 1, size, 1 do
			table.insert( land[i], " " )
		end
	end
	return land
end

function ECO:proTime()
	self.time = self.time + self.speed
end

function ECO:setTime( val )
	self.time = self.time + val
end

function ECO:Objects()
	return self.objects
end

function ECO:removeObj( obj )
	local id = obj:ID()
	for k, v in pairs( self.objects ) do
		if ( v:ID() == id ) then
			self.objects[k] = nil
		end
	end
end

function ECO:getMax()
	return math.floor( self.size / 1000 )
end

function ECO:getSize()
	return self.size
end

function ECO:getTime()
	return self.time
end

function ECO:findPrey()
	local num = 0
	for k, v in pairs( self.objects ) do
		if v:Type() == "prey" then
			num = num + 1
		end
	end
	return num
end

function ECO:findPred()
	local num = 0
	for k, v in pairs( self.objects ) do
		if v:Type() == "predator" then
			num = num + 1
		end
	end
	return num
end

function ECO:ratio()
	return self:findPred() / self:findPrey()
end

function ECO:addon( pos )
	if ( pos == 1 ) then
		return "UID: "..u_id
	elseif ( pos == 2 ) then
		return "Predators: "..self:findPred()
	elseif ( pos == 3 ) then
		return "Prey: "..self:findPrey()
	elseif ( pos == 4 ) then
		return "Ratio: "..self:ratio()
	end
	return ""
end

function ECO:disLand()
	local land = self:makeLand( math.floor( self.size / 1000 ) )
	for k, v in pairs( self.objects ) do
		local x, y = v:getPos()
		x, y = math.floor( x/1000 ), math.floor( y/1000 )
		if x < 1 then x = 1 end 
		if y < 1 then y = 1 end
		if x > math.floor(self.size/1000) then x = math.floor(self.size/1000) end
		if y > math.floor(self.size/1000) then y = math.floor(self.size/1000) end
		if type( x ) ~= "number" then x = 1 end
		if type( y ) ~= "number" then y = 1 end
		--X PLACEMENT--
		if ( land ~= nil ) then
			--print( x, y, v:ID(), v:Type().."	", v:getPos() )
			--os.execute( 'echo \"'..tostring( x ).." "..tostring( y )..'\" >> file' )
			if ( type( land[x][y] ) == "table" ) then
				local rand = math.random( 0, 1 )
				if ( rand == 1 ) then
					if ( x == 1 ) then
						x = x + 1
					elseif ( x == self.size / 1000 ) then
						x = x - 1
					else
						rand = math.random( -1, 1 )
						if ( rand == 0 ) then rand = 1 end
						x = x + rand
					end
				else
					--Y PLACEMENT--
					if ( y == 1 ) then
						y = y + 1
					elseif ( y == self.size / 1000 ) then
						y = y - 1
					else
						rand = math.random( -1, 1 )
						if ( rand == 0 ) then rand = 1 end
						y = y + rand
					end
				end
			end
		end
		land[x][y] = v
	end
	os.execute( 'clear')
	print("----------------------------------------------------------------------------------------------------")
	for k, v in pairs( land ) do
		local dLand = ""
		for _, pos in pairs( v ) do
			if ( type( pos ) == "table" ) then
				if ( pos:Type() == "predator" ) then
					if ( pos:alive() ) then
						dLand = dLand.."P "
					else
						dLand = dLand.."X "
					end
				elseif ( pos:Type() == "prey" ) then
					if ( pos:alive() ) then
						dLand = dLand.."o "
					end
				elseif ( pos:Type() == "foodO" ) then
					dLand = dLand.."F "
				elseif ( pos:Type() == "foodP" ) then
					dLand = dLand.."X "
				else
					dLand = dLand.."?"
				end
			else
				dLand = dLand.."  "
			end
		end
		print( dLand.."|"..self:addon( k ) )
	end
end

function ECO:addPred( age, x, y )
	local object = PREDATOR( age )
	if ( object ~= nil ) then
		if x == nil then
			x = math.random( self.size )
		end
		if y == nil then
			y = math.random( self.size )
		end
		object:setPos( x, y )
		self.objects[ tostring(object:ID() ) ] = object
	end
end

function ECO:addPrey( age, x, y )
	local object = PREY( age )
	if ( object ~= nil ) then
		if x == nil then
			x = math.random( self.size )
		end
		if y == nil then
			y = math.random( self.size )
		end
		object:setPos( x, y )
		self.objects[ tostring(object:ID() ) ] = object
	end
end

function ECO:dropFood()
	local obj = FOOD()
	x = math.random( self.size )
	y = math.random( self.size )
	obj:setPos( x, y )
	self.objects[ tostring( obj:ID() ) ] = obj
end