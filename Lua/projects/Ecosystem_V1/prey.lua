--[[
PREY FILE
]]--

require( 'animal' )

PREY = {}
PREY.__index = PREY

setmetatable( PREY, {
	__index = ANIMAL,
	__call = function ( cls, ... )
		local self = setmetatable( {}, cls )
		self:_init( ... )
		return self
	end
} )

function PREY:_init( age, agro, speed )
	ANIMAL._init( self, age, agro, speed )
	self._type = "prey"
end

function PREY:think()
	local food = self:findObj( "foodO", 20000 )
	local pred = self:findObj( "predator", 5000 )
	if ( pred ) then
		self:runTo( self, pred )
	elseif ( food ) then
		self:goTo( self, food )
	else
		self:wander()
	end
end

function PREY:goTo( obj, targ )
	local x, y = obj:getPos()
	local _x, _y = targ:getPos()
	local Cx = ( _x - x )
	local Cy = ( _y - y )
	local h = math.sqrt( Cx^2 + Cy^2 )
	local speed = self.speed
	local r = speed / h
	if ( r < 1 ) then
		Cx = Cx * r
		Cy = Cy * r
		self:setPos( self.pos.x + Cx, self.pos.y + Cy )
	else
		self:setPos( _x, _y )
		self:eat( targ )
	end
end

function PREY:runTo( obj, targ )
	local x, y = obj:getPos()
	local _x, _y = targ:getPos()
	local Cx = ( _x - x )
	local Cy = ( _y - y )
	local h = math.sqrt( Cx^2 + Cy^2 )
	local speed = self.speed
	local r = speed / h
	if ( h <= 0 ) then
		return 
	else
		Cx = Cx * r
		Cy = Cy * r
		nX = self.pos.x - Cx
		nY = self.pos.y - Cy
		if nX < 1 then nX = 1 end
		if nY < 1 then nY = 1 end
		if nX > ecosystem:getSize() then nX = ecosystem:getSize() end
		if nY > ecosystem:getSize() then nY = ecosystem:getSize() end
		self:setPos( nX, nY ) 
	end
end

function PREY:eat( targ )
	ecosystem:removeObj( targ )
	ecosystem:addPrey( 40, self:getPos() )
	self.dAge = self.dAge + 2
end