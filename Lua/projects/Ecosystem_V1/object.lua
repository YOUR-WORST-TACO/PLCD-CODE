--[[
OBJECT FILE
]]--

OBJ = {}
OBJ.__index = OBJ

setmetatable( OBJ, {
	__call = function ( cls, ... )
		local self = setmetatable( {}, cls )
		self:_init( ... )
		return self
	end
} )

function OBJ:_init()
	self.id = uid()
	self.pos = {x=0,y=0}
end

function OBJ:setPos( x, y )
	self.pos.x = math.floor( x )
	self.pos.y = math.floor( y )
end

function OBJ:getPos()
	return self.pos.x, self.pos.y
end

function OBJ:Type()
	return self._type
end

function OBJ:ID()
	return self.id
end

--OTHER CLASSES BASED OFF OF THIS--
FOOD = {}
FOOD.__index = FOOD

setmetatable( FOOD, {
	__index = OBJ,
	__call = function ( cls, ... )
		local self = setmetatable( {}, cls )
		self:_init( ... )
		return self
	end
} )

function FOOD:_init()
	OBJ._init( self )
	self._type = "foodO"
end