CommandsList = { "Open - Open a file that already exists.", "New - Create a new file", "Help - Displays all commands and usage", "Readline - Reads a singular line or the entire file.", "Encrypt - Takes a file and encrypts it. (Nuff said)", "Addline - Add a new line of text", "End - Terminates the program" }
Version = "0.0.1"
local fileName = nil
local openFile = nil
local fileContents = {}
function main()
  while true do
    Input = string.lower( io.read() )
    if Input == "open" then       --opens and reads a file
      open()
    elseif Input == "new" then    --creates a new file
      create()
    elseif Input == "addline" then    --adds a new line of text to the file
      addline()
    elseif Input == "end" then
      break
    elseif Input == "decrypt" then
      dFile()
    elseif Input == "encrypt" then
      eFile()
    elseif Input == "closefile" then
      closeFile()
    elseif Input == "readline" then
      readLine()
    elseif Input == "help" then   --returns allowed commands
      endstr = ""
      for k,v in pairs( CommandsList ) do
        endstr = endstr .. ( "\n\t"..v )
      end
      print( "Allowed commands are:"..endstr )
    else
      print( "Unknown command. You can find commands using help." )
    end
  end
end

function create()
  print( "New FileName(Without file type): " )
  fName = io.read()..".txt"
  if not exists( fName ) then
    fileName = fName
    io.open( fName, "w" )
    openFile = io.open( fName, "a" )
    print( "Password? :" )
    pass = io.read()
    openFile:write( recode( pass ).."\n" )
    openFile:close()
    openFile = nil
    read()
    print( "File, "..fName..", has been created." )
  else
    print( "File already exists." )
  end
end

function closeFile()
  if openFile == nil then
    print( "There is no file currently open." )
  else
    openFile:close()
    openFile = nil
    fileName = nil
    fileContents = {}
  end
end

function dFile()
  print( "File name(Without file type): " )
  fName = io.read()..".txt"
  if exists( fName ) then
    print( "New File Name(With file type):" )
    nName = io.read()
    exist, nFile = exists( nName )
    if exist then
      fileContents = {}
      openFile = io.open( fName, "a+" )
      first = true
      for line in openFile:lines() do
        if first then
          first = false
        else
          table.insert( fileContents, line )
        end
      end
      addstr = ""
      print("pass")
      for k, v in pairs( fileContents ) do
        addstr = addstr..decode( fileContents[k] ).."\n"
        print("pass"..k)
      end
      nFile:write(addstr)
      nFile:close()
      closeFile()
      print("done")
    else
      closeFile()
      print("File already exists")
    end
  end
end

function eFile()
  print( "File name(With file type): " )
  fName = io.read()
  if exists( fName ) then
    print( "New File Name(Without file type):" )
    nName = io.read()..".txt"
    exist, nFile = exists( nName )
    if exist then
      fileContents = {}
      openFile = io.open( fName, "a+" )
      first = true
      for line in openFile:lines() do
        if first then
          first = false
        else
          table.insert( fileContents, line )
        end
      end
      addstr = ""
      for k, v in pairs( fileContents ) do
        addstr = addstr..recode( fileContents[k] ).."\n"
        print("pass"..k)
      end
      nFile:write(addstr)
      nFile:close()
      closeFile()
      print("done")
    else
      closeFile()
      print("File already exists")
    end
  end
end

function readLine()
  if openFile == nil then print( "No file loaded." ) return end
  if tblLen( fileContents ) == 0 then print( "File is empty." ) return end
  print( "Line Number: (a for all)" )
  line = io.read()
  if line == "a" then
    for k,v in pairs( fileContents ) do
      print( k..": "..decode( v ) )
    end
  else
    print( decode( fileContents[tonumber(line)] ) )
  end
end

function deleteLine()
  if tbLen( fileContents ) == 0 then
    print( "There is nothing to be deleted." )
    return
  end
  for k,v in pairs( fileContents ) do
    print( k..": "..decode( v ) )
  end
  print( "What line do you wish to remove?" )
  line = io.read()
  tempTbl = {}
  for k, v in pairs( fileContents ) do
    if k == tonumber( line ) then
    else
      table.insert( tempTbl, fileContents[ k ] )
    end
  end

end

function tblLen( tbl )
  retnum = 0
  for k,v in pairs( tbl ) do
    retnum = retnum+1
  end
  return retnum
end

function addline()
  print("Line Contents? (Includes a-z lowercase, and 1-9)")
  line = io.read()
  openFile:write( recode( line ).."\n" )
  openFile:close()
  openFile = nil
  read( openFile )
  print( "Line added." )
end

function password( file )
  pas = decode( file:read() )
  if not pas then return true end
  print( "Please enter the password for this file" )
  answer = string.lower(io.read())
  if answer == pas then
    return true
  end
  print( "Incorrect Password" )
  closeFile()
  return false
end

function read( fName )
  fileContents = {}
  openFile = io.open( fName, "a+" )
  first = true
  for line in openFile:lines() do
    if first then
      first = false
    else
      table.insert( fileContents, line )
    end
  end
end

function open()
  print( "FileName(With file type): ")
  fName = io.read()
  exist, oFile = exists( fName )
  if exist then
    fileName = fName
    if not password( oFile ) then return end
    read( fileName )
    print( "File loaded" )
  else
    print( "No Such File" )
  end
end

function decode( text )
  retstr = ""
  if not text then return end
  for i=1, string.len( text ), 3 do
    sub = string.char( tonumber( string.sub( text, i, i+2 ) ) )
    retstr = retstr..sub
  end
  return retstr
end

function recode( text )
  retstr = ""
  for i=1, string.len( text ), 1 do
    sub = string.byte( string.sub( text, i, i ) )
    if sub < 10 then
      retstr = retstr.."00"..sub
    elseif sub < 100 then
      retstr = retstr.."0"..sub
    else
      retstr = retstr..sub
    end
  end
  return retstr
end

function exists( file )
  oFile = io.open( file, "a+" )
  if oFile == nil then
    return false
  end
  return true, oFile
end

function write()
  print( "Write" )
end

print( "TACO-Cipher V."..Version )
main()