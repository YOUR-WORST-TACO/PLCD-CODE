require( 'include' )

function sleep( time )
	local t0 = os.clock()
	while os.clock() - t0 <= time do end
end

gamedata = GameCreation()
gamedata.vars.gamenum = 0
gamedata.init = function( self )
	self.deck:Shuffle( 100 )
	self.deck:Cut()
	self.deck:Deal( 30, self.players )
	self.gamenum = self.gamenum + 1
	self.finish = 0
	self.curent = 1
	self.take = nil
	self.turn = function()
		local len = taco.len( self.players )
		self.curent = self.curent + 1
		if self.curent > len then
			self.curent = 1
		end
	end
	self.DoSlap = function()
		local rand = math.random( 1, taco.len( self.players ) + 1 )
		if rand <= taco.len( self.players ) then
			local ply = self.players[rand]
			for k, v in pairs( self.deck.discard ) do
				table.insert( ply.hand, 1, v )
				self.deck.discard[k] = nil
			end
			sleep( 0.5 )
			taco.Alert( ply:Nick().." Slapped" )
			return true
		end
		return false
	end
	self.IsSlap = function()
		local top = self.deck.discard[ taco.len( self.deck.discard ) ]
		local second = nil
		local third = nil
		if taco.len( self.deck.discard ) >= 2 then
			second = self.deck.discard[ taco.len( self.deck.discard ) - 1 ]
		end
		if taco.len( self.deck.discard ) >= 3 then
			third = self.deck.discard[ taco.len( self.deck.discard ) - 2 ]
		end
		if ( top ~= nil and second ~= nil ) then
			if ( top.symb == second.symb ) then
				return self:DoSlap()
			elseif ( third ~= nil ) then
				if ( top.symb == third.symb ) then
					return self:DoSlap()
				end
			end
		end
		return false
	end
end
gamedata.think = function( self )
	if self.finish > 0 then
		local ply = self.players[self.curent]
		if ( ply:GetAmnt() > 0 ) then
			ply:Discard( 1, self.deck:GetDiscard(), false )
			local top = self.deck.discard[ taco.len( self.deck.discard ) ]
			print( self.gamenum, ply:Nick(), top:GetFace() )
			top:Face()
			print("")
			if self:IsSlap() then
				self.finish = 0
			elseif top:IsFace() then
				self.take = ply
				if top.symb == 11 then
					self.finish = 2
				elseif top.symb == 12 then
					self.finish = 3
				elseif top.symb == 13 then
					self.finish = 4
				else
					self.finish = 5
				end
				self:turn()
			elseif ( self.finish == 1 ) then
				for k, v in pairs( self.deck.discard ) do
					table.insert( self.take.hand, 1, v )
					self.deck.discard[k] = nil
				end
				sleep( 0.5 )
				taco.Alert( "Round goes to "..self.take:Nick())
			end
			sleep( 0.5 )
			self.finish = self.finish - 1
		else
			self:turn()
		end
	else
		local ply = self.players[self.curent]
		if ( ply:GetAmnt() > 0 ) then
			ply:Discard( 1, self.deck:GetDiscard(), false )
			local top = self.deck.discard[ taco.len( self.deck.discard ) ]
			print( self.gamenum, ply:Nick(), top:GetFace() )
			top:Face()
			print("")
			self:turn()
			if self:IsSlap() then
				self.finish = 0
			elseif top:IsFace() then
				self.take = ply
				if top.symb == 11 then
					self.finish = 1
				elseif top.symb == 12 then
					self.finish = 2
				elseif top.symb == 13 then
					self.finish = 3
				else
					self.finish = 4
				end
			end
			sleep( 0.5 )
		else
			self:turn()
		end
	end
end
gamedata.win = function( self )
	local hascards = 0
	local win = ""
	for k, v in pairs( self.players ) do
		if v:GetAmnt() > 0 then
			win = v:Nick()
			hascards = hascards + 1
		end
	end
	if hascards == 1 and self.finish == 0 then
		taco.Alert( win.." Has Won Game "..self.gamenum )
		for k, v in pairs( self.players ) do
			v:Discard( v:GetAmnt(), self.deck.discard )
		end
		self.deck:Shuffle( 100 )
		self:init()
		sleep( 10 )
	end
end
gamedata.deck = DECK( false )
gamedata.players = { 
	"Taco",
	"Degen",
	"Mikidy",
	"Lya"
}

slap = GAME( gamedata )
slap:Start()