DECK = {}
DECK.__index = DECK

setmetatable( DECK, {
	__call = function ( cls, ... )
		local self = setmetatable( {}, cls )
		self:_init( ... )
		return self
	end
} )

function DECK:_init( joker )
	self.deck = self:MakeDeck( joker )
	self.discard = {}
end

function DECK:MakeDeck( bool )
	local deck = {}
	local jokers = 0
	if bool then jokers = 2 end
	for i = 1, 4, 1 do
		for k = 1, 13, 1 do
			table.insert( deck, CARD( i, k ) )
		end
	end
	for i = 1, jokers, 1 do
		table.insert( deck, CARD( 5, 14 ) )
	end
	return deck
end

function DECK:Deal( amnt, recievers )
	local disc = taco.len( self.deck ) % taco.len( recievers )
	for i = 1, amnt, 1 do
		for _, ply in pairs( recievers ) do
			if taco.len( self.deck ) > disc then
				ply:Add( self.deck[ taco.len( self.deck ) ] )
				self.deck[ taco.len( self.deck ) ] = nil
			else
				self.discard = self.deck
				self.deck = {}
				return
			end
		end
	end
end

function DECK:GetCards( start, stop )
	local tbl = {}
	for k, v in pairs( self.deck ) do
		if k >= start and k <= stop then
			table.insert( tbl, v )
		end
	end
	return tbl
end

function DECK:GetDiscard()
	return self.discard
end

function DECK:Shuffle( amnt )
	if amnt == nil then amnt = 7 end
	for i = 1, taco.len( self.discard ), 1 do
		table.insert( self.deck, self.discard[ i ] )
	end
	self.discard = {}
	for i = 1, amnt, 1 do
		local newDeck = {}
		local len = taco.len
		local pos = math.floor( taco.len( self.deck )/2 )
		local lDeck = self:GetCards( 1, pos )
		local rDeck = self:GetCards( pos + 1, taco.len( self.deck ) )
		while ( len( lDeck ) ~= 0 or len( rDeck ) ~= 0 ) do

			local rand = math.random( 0, 1 )
			if ( len( lDeck ) ~= 0 and len( rDeck ) ~= 0 ) then
				if rand == 0 then
					table.insert( newDeck, lDeck[ len( lDeck ) ] )
					lDeck[ len( lDeck ) ] = nil
				else
					table.insert( newDeck, rDeck[ len( rDeck ) ] )
					rDeck[ len( rDeck ) ] = nil
				end
			elseif ( len( lDeck ) ~= 0 ) then
				table.insert( newDeck, lDeck[ len( lDeck ) ] )
				lDeck[ len( lDeck ) ] = nil
			else 
				table.insert( newDeck, rDeck[ len( rDeck ) ] )
				rDeck[ len( rDeck ) ] = nil
			end
		end
		self.deck = newDeck
	end
end

function DECK:Cut()
	local rand = math.random( taco.len( self.deck ) )
	local tDeck = self:GetCards( 1, rand )
	local bDeck = self:GetCards( rand + 1, taco.len( self.deck ) )
	for i = 1, taco.len( tDeck ), 1 do
		table.insert( bDeck, tDeck[ i ] )
	end
	self.deck = bDeck
end

function DECK:Display()
	print( "----DECK----")
	for k, v in pairs( self.deck ) do
		print( k, v:GetFace())
	end
	print( "" )
	print( "----DISCARD----" )
	for k, v in pairs( self.discard ) do
		print( k, v:GetFace() )
	end
end

function DECK:Count( sep )
	local num = 0
	sep = string.lower( sep )
	for k, v in pairs( self.deck ) do
		local name = string.lower( v:GetFace() )
		for i = 1, string.len( name ) - string.len( sep ) + 1, 1 do
			local sub = string.sub( name, i, i+string.len( sep ) - 1 )
			if sub == sep then
				num = num + 1
			end
		end
	end
	return num
end