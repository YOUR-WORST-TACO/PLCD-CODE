CARD = {}
CARD.__index = CARD

setmetatable( CARD, {
	__call = function ( cls, ... )
		local self = setmetatable( {}, cls )
		self:_init( ... )
		return self
	end
} )

function CARD:_init( suit, symb )
	self.suit = suit
	self.symb = symb 
	self.card = self:ConstructFace()
	self.face = self:FACE()
end

function CARD:FACE()
	local xx = self.symb
	local yy = ""
	local a = self.suit
	if xx == 11 then
		xx = "J "
		yy = " J"
	elseif xx == 12 then
		xx = "Q "
		yy = " Q"
	elseif xx == 13 then
		xx = "K "
		yy = " K"
	elseif xx == 1 then
		xx = "A "
		yy = " A"
	elseif xx == 10 then
		xx = "10"
		yy = "10"
	elseif xx == 14 then
		xx = "JK"
		yy = "JK"
	else
		yy = " "..xx
		xx = xx.." "
	end
	local suits = { 
		{
			" O ",
			"O O",
			" | "
		},
		{
			"O O",
			"\\ /",
			" V "
		},
		{
			"/\\ ",
			"\\/ ",
			"   "
		},
		{
			" ^ ",
			"O O",
			" | "
		},
		{
			"   ",
			"JKR",
			"   "
		}
	}
	local cardbase = {
		" ------- ",
		"|"..xx.."     |",
		"|  "..suits[a][1].."  |",
		"|  "..suits[a][2].."  |",
		"|  "..suits[a][3].."  |",
		"|     "..yy.."|",
		" -------"
	}
	return cardbase
end

function CARD:Face()
	if self.face ~= nil then
		for i = 1, 7, 1 do
			print( self.face[i] )
		end
	end
end

function CARD:ConstructFace()
	local suit = {"Clubs","Hearts","Diamonds","Spades"}
	local names = {"Ace", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten", "Jack", "Queen", "King", "JOKER"}
	if self.suit == 5 then
		return names[self.symb]
	end
	return names[self.symb].." of "..suit[self.suit]
end

function CARD:GetFace()
	return self.card
end

function CARD:IsFace()
	if (self.symb >= 11 and self.symb <= 13) or self.symb == 1 then
		return true
	end
	return false
end

function CARD:Info()
	return self.symb, self.suit
end