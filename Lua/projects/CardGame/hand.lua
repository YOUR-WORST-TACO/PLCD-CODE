HAND = {}
HAND.__index = HAND

setmetatable( HAND, {
	__call = function ( cls, ... )
		local self = setmetatable( {}, cls )
		self:_init( ... )
		return self
	end
} )

function HAND:_init( nick )
	self.hand = {}
	self.nick = nick
end

function HAND:Add( card )
	table.insert( self.hand, card )
end

function HAND:Sort( type )

end

function HAND:Nick()
	return self.nick
end

function HAND:GetAmnt()
	return taco.len( self.hand )
end

function HAND:GetCard()
	return self.hand[taco.len( self.hand ) ]
end

function HAND:RemoveCard()
	local card = self.hand[taco.len( self.hand ) ]
	self.hand[taco.len( self.hand ) ] = nil
	return card
end

function HAND:Discard( num, place, bottom )
	for i = 1, num, 1 do
		if bottom then
			table.insert( place, 1, self:RemoveCard() )
		else
			table.insert( place, self:RemoveCard() )
		end
	end
end