--[[
YOUR WORST TACO's LUA LIBRARY FOR LUA APPLICATIONS
Yay Look I did it!
]]--

taco = {}

function taco.Explode(inputstr, sep)
    if sep == nil then
            sep = "%s"
    end
    local t={} ; i=1
    for str in string.gmatch(inputstr, "([^"..sep.."]+)") do
        t[i] = str
        i = i + 1
    end
    return t
end

function taco.len( inp )
	local inc = 0
	for _, v in pairs( inp ) do
		inc = inc + 1
	end
	return inc
end

function taco.strClean( str, ident )
	local cleanstr = ""
	if type( ident ) == "table" then
		for k, v in pairs( ident ) do
			for i = 1, string.len( str ), 1 do
				local substr = string.sub( str, i, i )
				if substr ~= v then
					cleanstr = cleanstr..substr
				end
			end
			str=cleanstr
			cleanstr = ""
		end
	else
		for i = 1, string.len( str ), 1 do
			local substr = string.sub( str, i, i )
			if substr ~= ident then
				cleanstr = cleanstr..substr
			end
		end
		str=cleanstr
	end
	return str
end

function taco.Combine( intbl, com )
	local retstr = ""
	for _, v in pairs( intbl ) do
		if taco.len( intbl ) > _ then
			retstr = retstr..v..com
		else
			retstr = retstr..v
		end
	end
	return retstr
end

function printTable( tbl, lvl )
	if lvl == nil then 
		lvl = 1
		print( tabss( lvl -1 )..tostring( tbl ).."{"  )
	end
	for _, v in pairs( tbl ) do
		if type( v ) == "table" then
			print( tabss( lvl )..tostring( _ ).."{" )
			printTable( v, lvl + 1 )
		else
			print( tabss( lvl ).._.."--"..tostring( v ).."    ("..type(v)..")" )
		end
	end
	print( tabss( lvl -1 ).."}")
end

function tabss( num )
	retstr = ""
	for i = 1, num, 1 do
		retstr = retstr.."  "
	end
	return retstr
end

function taco.iNum( arg )
	arg = tonumber( arg )
	if type( arg ) == "number" then
		return true
	end
	return false
end

function taco.Alert( msg )
	local str = {"","","","",""}
	for i = 1, string.len( msg ), 1 do
		local sub = string.byte(string.upper(string.sub( msg, i, i )))
		if alphebet[sub] ~= nil then
			str[1] = str[1]..alphebet[sub][1]
			str[2] = str[2]..alphebet[sub][2]
			str[3] = str[3]..alphebet[sub][3]
			str[4] = str[4]..alphebet[sub][4]
			str[5] = str[5]..alphebet[sub][5]
		end
	end
	print( "" )
	print( str[1] )
	print( str[2] )
	print( str[3] )
	print( str[4] )
	print( str[5] )
	print( "" )
end

do
	alphebet = {}
	--[[
	alphebet[x] = {
		"00000",
		"00000",
		"00000",
		"00000",
		"00000"
	}
	]]--
	alphebet[32] = {
		"  ",
		"  ",
		"  ",
		"  ",
		"  "
	}

	alphebet[48] = {
		" 000  ",
		"00  0 ",
		"0 0 0 ",
		"0  00 ",
		" 000  "
	}
	alphebet[49] = {
		"  0   ",
		" 00   ",
		"  0   ",
		"  0   ",
		"00000 "
	}
	alphebet[50] = {
		"00000 ",
		"    0 ",
		"00000 ",
		"0     ",
		"00000 "
	}
	alphebet[51] = {
		"00000 ",
		"    0 ",
		" 0000 ",
		"    0 ",
		"00000 "
	}
	alphebet[52] = {
		"   00 ",
		"  0 0 ",
		"00000 ",
		"    0 ",
		"    0 "
	}
	alphebet[53] = {
		"00000 ",
		"0     ",
		"0000  ",
		"    0 ",
		"0000  "
	}
	alphebet[54] = {
		"  00  ",
		" 0    ",
		"0000  ",
		"0   0 ",
		" 000  "
	}
	alphebet[55] = {
		"00000 ",
		"   0  ",
		"  0   ",
		" 0    ",
		"0     "
	}
	alphebet[56] = {
		" 000  ",
		"0   0 ",
		" 000  ",
		"0   0 ",
		" 000  "
	}
	alphebet[57] = {
		"00000 ",
		"0   0 ",
		"00000 ",
		"    0 ",
		"00000 "
	}

	alphebet[65] = {
		"  0   ",
		" 0 0  ",
		"0   0 ",
		"00000 ",
		"0   0 "
	}
	alphebet[66] = {
		"0000  ",
		"0   0 ",
		"0000  ",
		"0   0 ",
		"00000 "
	}
	alphebet[67] = {
		"00000 ",
		"0     ",
		"0     ",
		"0     ",
		"00000 "
	}
	alphebet[68] = {
		"0000  ",
		"0   0 ",
		"0   0 ",
		"0   0 ",
		"0000  "
	}
	alphebet[69] = {
		"00000 ",
		"0     ",
		"0000  ",
		"0     ",
		"00000 "
	}
	alphebet[70] = {
		"00000 ",
		"0     ",
		"0000  ",
		"0     ",
		"0     "
	}
	alphebet[71] = {
		"00000 ",
		"0     ",
		"0  00 ",
		"0   0 ",
		"00000 "
	}
	alphebet[72] = {
		"0   0 ",
		"0   0 ",
		"00000 ",
		"0   0 ",
		"0   0 "
	}
	alphebet[73] = {
		"00000 ",
		"  0   ",
		"  0   ",
		"  0   ",
		"00000 "
	}
	alphebet[74] = {
		"00000 ",
		"  0   ",
		"  0   ",
		"0 0   ",
		"000   "
	}
	alphebet[75] = {
		"0   0 ",
		"0  0  ",
		"000   ",
		"0  0  ",
		"0   0 "
	}
	alphebet[76] = {
		"0     ",
		"0     ",
		"0     ",
		"0     ",
		"00000 "
	}
	alphebet[77] = {
		"00000 ",
		"0 0 0 ",
		"0 0 0 ",
		"0 0 0 ",
		"0 0 0 "
	}
	alphebet[78] = {
		"0   0 ",
		"00  0 ",
		"0 0 0 ",
		"0  00 ",
		"0   0 "
	}
	alphebet[79] = {
		"00000 ",
		"0   0 ",
		"0   0 ",
		"0   0 ",
		"00000 "
	}
	alphebet[80] = {
		"00000 ",
		"0   0 ",
		"00000 ",
		"0     ",
		"0     "
	}
	alphebet[81] = {
		" 000  ",
		"0   0 ",
		"0   0 ",
		"0  00 ",
		" 00 0 "
	}
	alphebet[82] = {
		"0000  ",
		"0   0 ",
		"0000  ",
		"0   0 ",
		"0   0 "
	}
	alphebet[83] = {
		"00000 ",
		"0     ",
		"00000 ",
		"    0 ",
		"00000 "
	}
	alphebet[84] = {
		"00000 ",
		"  0   ",
		"  0   ",
		"  0   ",
		"  0   "
	}
	alphebet[85] = {
		"0   0 ",
		"0   0 ",
		"0   0 ",
		"0   0 ",
		"00000 "
	}
	alphebet[86] = {
		"0   0 ",
		"0   0 ",
		"0   0 ",
		" 0 0  ",
		"  0   "
	}
	alphebet[87] = {
		"0   0 ",
		"0 0 0 ",
		"0 0 0 ",
		"0 0 0 ",
		"00000 "
	}
	alphebet[88] = {
		"0   0 ",
		" 0 0  ",
		"  0   ",
		" 0 0  ",
		"0   0 "
	}
	alphebet[89] = {
		"0   0 ",
		" 0 0  ",
		"  0   ",
		"  0   ",
		"  0   "
	}
	alphebet[90] = {
		"00000 ",
		"   0  ",
		"  0   ",
		" 0    ",
		"00000 "
	}
end