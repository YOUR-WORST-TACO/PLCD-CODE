GAME = {}
GAME.__index = GAME

setmetatable( GAME, {
	__call = function ( cls, ... )
		local self = setmetatable( {}, cls )
		self:_init( ... )
		return self
	end
} )

function GameCreation()
	local tbl = {}
	tbl.think = function() end
	tbl.win = function() end
	tbl.init = function() end
	tbl.deck = DECK( true )
	tbl.players = {}
	tbl.vars = {}
	return tbl
end

function GAME:_init( c )
	self.init = loadstring(string.dump( c.init ))
	self.Think = loadstring(string.dump( c.think ))
	self.win = loadstring(string.dump( c.win ))
	self.deck = c.deck
	self.players = {}
	self.run = true
	for k, v in pairs( c.vars ) do
		self[k] = v
	end
	self.thinkTime = 0
	for k, v in pairs( c.players ) do
		table.insert( self.players, HAND( v ) )
	end
	--ONCE DONE LOADING EVERYTHING FOR THE GAME CALL INIT--
	self:init()
end

function GAME:Start()
	while self:win() ~= true and self.run do
		self:Think()
	end
end